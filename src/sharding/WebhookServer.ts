import express, {Express} from 'express';
import {Webhook as TopggWebhook, WebhookPayload} from '@top-gg/sdk';
import http from 'http';
import https from 'https';
import fs from 'fs';

import Frodo from './Frodo';
import {__dirname} from './../../loadEnvVars.js';

export default class WebhookServer {
	app: Express;
	server: http.Server | https.Server;
	topggWebhook: TopggWebhook;
	frodo: Frodo;

	constructor(frodo: Frodo) {
		this.frodo = frodo;
		if (!process.env.TOPGG_WEBHOOK_AUTH) {
			this.frodo.logger.warn('No top.gg webhook auth found (set TOPGG_WEBHOOK_AUTH in .env)');
			return;
		}
		this.app = express();
		this.server = this.createServer();
		this.topggWebhook = new TopggWebhook(process.env.TOPGG_WEBHOOK_AUTH);
		this.frodo.logger.info('Starting webhook server');
		this.initaliseRoutes();
		this.start();
	}

	private createServer(): http.Server | https.Server {
		if (process.env.WEBHOOK_SSL_CERT && process.env.WEBHOOK_SSL_KEY) {
			this.frodo.logger.info('Starting webhook server with SSL');
			const cert = fs.readFileSync(`${__dirname}/../${process.env.WEBHOOK_SSL_CERT}`);
			const key = fs.readFileSync(`${__dirname}/../${process.env.WEBHOOK_SSL_KEY}`);
			return https.createServer({
				cert,
				key,
				ca: process.env.WEBHOOK_SSL_CA_CERT ? fs.readFileSync(`${__dirname}/../${process.env.WEBHOOK_SSL_CA_CERT}`) : undefined,
			}, this.app);
		} else {
			this.frodo.logger.info('Starting webhook server without SSL');
			return http.createServer(this.app);
		}
	}

	private initaliseRoutes() {
		this.app.post('/topgg', this.topggWebhook.listener((vote) => {
			this.frodo.sendDm(vote);
		}));
	}

	private start() {
		this.server.listen(process.env.WEBHOOK_PORT || 3000, () => {
			this.frodo.logger.info(`Webhook server started on port ${process.env.WEBHOOK_PORT || 3000}`);
		});
	}
}

export {WebhookPayload as TopggVote};
