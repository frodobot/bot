export const StatusTypes = [
	'PLAYING',
	'STREAMING',
	'LISTENING',
	'WATCHING',
	'COMPETING',
];
