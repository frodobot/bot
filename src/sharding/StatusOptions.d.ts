export interface StatusOptions {
	type: string;
	status: string;
	url?: string;
}
