// Handles the WebSocket connection to the server

import {RawData, WebSocket} from 'ws';

import Frodo from './Frodo.js';
import getJson from '../core/GetJson.js';
import wait from '../core/Wait.js';

export default class WebSocketManager {
	frodo: Frodo;
	webSocket: WebSocket;
	live: boolean;
	connectTries: number;
	url: string;
	reconnecting: boolean;
	maxConnectTries: number;
	connected: boolean;
	ping: number;
	lastMessageSent: number;
	auth: string;
	waitInterval: number;

	constructor(live: boolean, frodo: Frodo, auth: string) {
		this.frodo = frodo;
		if (!auth) {
			this.frodo.logger.warn('No auth token provided for WebSocket (set WEBSOCKETAUTH in .env)');
			return;
		}
		this.auth = auth;
		this.live = live;
		this.reconnecting = false;
		this.connected = false;
		this.ping = 0;
		this.lastMessageSent = 0;

		// Defines the amount of tries to connect and interval between unsuccessful tries
		this.maxConnectTries = 5;
		this.waitInterval = 60;

		this.url = live ? 's://frodo.fun' : '://localhost';
		this.connectTries = 0;
		this.startConnect(true);
	}

	private async getAuthToken(): Promise<string> {
		const auth = await getJson<{token: string}>(`http${this.url}/api/websockettoken`, {Authorization: this.auth});
		return auth.token;
	}

	private async connect() {
		const token = await this.getAuthToken();
		this.webSocket = new WebSocket(`ws${this.url}/${token}`);
		this.registerEvents();
	}

	private async startConnect(firstTime?: boolean) {
		this.connectTries = 1;
		this.reconnecting = true;
		if (!firstTime) this.frodo.logger.warn(`Starting to connect to WebSocket with ${this.maxConnectTries} attempts`);

		for (let i = 0; i < this.maxConnectTries; i++) {
			this.frodo.logger.info(`Attempting to connect to WebSocket server (${this.connectTries}/${this.maxConnectTries})`);

			try {
				await this.connect();
			} catch (e) {
				this.connectTries++;

				if (this.connectTries > this.maxConnectTries) {
					this.frodo.logger.error(`Failed to connect to WebSocket server ${this.maxConnectTries} times, not trying again`, e);
					return;
				}
				this.frodo.logger.error(`Failed to connect to WebSocket server, trying again in ${this.waitInterval} seconds`, e);

				await wait(this.waitInterval * 1000);
				continue;
			}
			this.frodo.logger.info(`${firstTime ? 'Connected' : 'Reconnected'} to WebSocket server`);
			break;
		}
	}

	private registerEvents() {
		this.webSocket.on('error', this.onError.bind(this));
		this.webSocket.on('open', this.onOpen.bind(this));
		this.webSocket.on('message', this.onMessage.bind(this));
		this.webSocket.on('close', this.onClose.bind(this));
	}

	private onError(error: Error) {
		this.frodo.logger.error('Error on WebSocket server', error);
		this.webSocket.close();

		if (this.reconnecting) return;
		this.startConnect();
	}

	private onOpen() {
		this.frodo.logger.info('WebSocket successfully opened');
		this.reconnecting = false;
		this.connected = true;
		this.startPing();
	}

	private async startPing() {
		while (!this.reconnecting) {
			if (this.frodo.manager.shards.size !== this.frodo.manager.shardList.length) {
				// If manager hasn't spawned all shards, send basic ping data
				this.webSocket.send('0:0:1');
			} else {
				this.webSocket.send(`${await this.frodo.getServerCount()}:${await this.frodo.getUserCount()}:${this.frodo.manager.shardList.length}`);
			}
			this.lastMessageSent = Date.now();
			await wait(30000);
		}
	}

	private onMessage(message: RawData) {
		try {
			const {type, data} = JSON.parse(message.toString());
			switch (type) {
			case 'pong':
				this.ping = Date.now() - this.lastMessageSent;
				break;
			case 'status':
				this.frodo.setStatus(data);
				break;
			}
		} catch (e) {}
	}

	private onClose() {
		this.frodo.logger.warn('WebSocket closed');
		this.connected = false;
		if (this.reconnecting) return;
		this.startConnect();
	}
}
