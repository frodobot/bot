import dotenv from 'dotenv';
dotenv.config();

import {Intents} from 'discord.js';
import {FrodoClient} from '../core/FrodoClient.js';

const client = new FrodoClient({
	intents: [
		Intents.FLAGS.GUILDS,
		Intents.FLAGS.GUILD_MESSAGES,
		Intents.FLAGS.DIRECT_MESSAGES,
	],
});
client.connectToDiscord();

export default client;
