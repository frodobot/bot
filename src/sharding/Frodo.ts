import {ShardingManager, ActivityOptions, MessageEmbed} from 'discord.js';

import createLogger, {Logger} from '../../Logger.js';
import {StatusOptions} from './StatusOptions.js';
import {StatusTypes} from './StatusTypes.js';
import WebhookServer, {TopggVote} from './WebhookServer.js';
import WebSocketManager from './WebSocketManager.js';
import {voteEmbed} from './../core/voteEmbed.js';
import {__dirname} from '../../loadEnvVars.js';

export default class Frodo {
	logger: Logger;
	manager: ShardingManager;
	webSocket: WebSocketManager;
	webhookServer: WebhookServer;
	lastUpdatedUserCount: number = 0;
	_userCount: number = 0;

	constructor() {
		this.logger = createLogger('MainProcess');
		this.logger.info('Frodo started');

		this.webSocket = new WebSocketManager(process.env.RUNTIME && !process.env.TESTING, this, process.env.WEBSOCKETAUTH);
		this.webhookServer = new WebhookServer(this);

		if (!process.env.TOKEN) {
			this.logger.error('No token found (set TOKEN in .env)');
			process.exit(1);
		}
		this.createShardingManager(process.env.TOKEN);
		this.spawnManager();
	}

	createShardingManager(token: string) {
		this.logger.info('Creating sharding manager');
		this.manager = new ShardingManager(`${__dirname}/src/sharding/bot.js`, {
			token,
			totalShards: 'auto',
		});
		this.manager.on('shardCreate', (shard) => {
			this.logger.info(`Shard ID ${shard.id} created`);
		});
		this.logger.info('Sharding manager created');
	}

	spawnManager() {
		this.manager.spawn();
		this.logger.info('Sharding manager spawned');
	}

	async setStatus(status: StatusOptions) {
		const statusOptions: ActivityOptions = {
			name: status.status || '/help',
			type: StatusTypes[status.type] || StatusTypes[0],
		};
		if (status.url) statusOptions.url = status.url;

		this.manager.broadcastEval<void, {statusOptions: ActivityOptions}>((client, {statusOptions}) => {
			client.user.setActivity(statusOptions);
		}, {context: {statusOptions}});
	}

	async getServerCount(): Promise<number> {
		try {
			const results = await this.manager.fetchClientValues('guilds.cache.size') as number[];
			return results.reduce((acc, guildCount) => acc + guildCount, 0);
		} catch (e) {
			this.logger.error('Failed to fetch server count', e);
			return 0;
		}
	}

	async getUserCount(): Promise<number> {
		if (this.lastUpdatedUserCount > Date.now() - 1000 * 60 * 5) return this._userCount;
		const results = await this.manager.broadcastEval((client) => client.guilds.cache.reduce((acc, guild) => acc + guild.memberCount, 0));
		this._userCount = results.reduce((acc, userCount) => acc + userCount, 0);
		this.lastUpdatedUserCount = Date.now();
		return this._userCount;
	}

	async sendDm(vote: TopggVote) {
		this.manager.broadcastEval<void, {vote: TopggVote, voteEmbed: MessageEmbed}>(async (client, {vote, voteEmbed}) => {
			try {
				const user = await client.users.fetch(vote.user);
				if (!user) return;
				await user.send({
					embeds: [voteEmbed],
				});
			} catch (e) {
				this.logger.error('Failed to send DM', e);
			}
		}, {context: {vote, voteEmbed}, shard: 0});
	}
}
