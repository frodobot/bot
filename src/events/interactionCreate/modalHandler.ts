import {MessageEmbed} from 'discord.js';

import {FrodoClient, Interaction} from './../../core/FrodoClient';
import {Event} from '../../core/Event.js';

export const event: Event = {
	name: 'interactionCreate',
	identifier: 'commandHandler',
	handler: modalHandler,
};

async function modalHandler(this: FrodoClient, interaction: Interaction) {
	if (!interaction.isModalSubmit()) return;

	const [interactionId, modalId] = interaction.customId.split(':');
	const command = this.interactionManager.getCommand(
		interactionId,
	);

	if (!command) {
		await interaction.reply({
			embeds: [
				new MessageEmbed()
					.setColor('#ff0000')
					.setDescription('Error: Please run the command again\nIf you think you have found a bug, please report it on https://help.frodo.fun'),
			],
			ephemeral: true,
			components: [],
		}).catch(() => {});
	} else {
		try {
			const res = command.onModalSubmit(modalId, interaction.fields, interaction);
			if (!res) {
				await interaction.deferUpdate();
			} else {
				await interaction.reply({
					content: res,
					ephemeral: true,
				});
			}
		} catch (e) {
			this.errorLog('Error in modal handler', e);
		}
	}
}
