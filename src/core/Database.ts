import {User, CommandInteraction} from 'discord.js';
import {Connection, createConnection, OkPacket} from 'mysql';

import {FrodoClient} from './FrodoClient';
import {LeaderboardSingleUser, LeaderboardItem} from './leaderboardItem.d';
import {Quiz} from './Quiz';
import {GameState} from './GameState.js';
import {DatabaseQuizResponse, QuizThumbnail} from './Quiz.d';

export default class Database {
	client: FrodoClient;
	connection: Connection;
	connected: boolean;

	constructor(discordClient: FrodoClient) {
		this.client = discordClient;
		this.connected = false;
		if (!process.env.DB_PASSWORD) {
			this.client.warnLog('No database password set, not connecting to database.');
			return;
		}
		this.connect();
	}

	private async connect() {
		if (this.connected) return;
		try {
			this.connection = createConnection({
				host: process.env.DB_SERVER,
				user: process.env.DB_USER,
				password: process.env.DB_PASSWORD,
				database: process.env.DB_DATABASE,
				port: parseInt(process.env.DB_PORT) || 3306,
				charset: 'utf8mb4',
			});
			this.connection.connect((err) => {
				if (err) {
					this.client.errorLog('Failed to connect to database', err);
					return;
				};
				this.connected = true;
				this.client.debugLog('Successfully connected to database');
			});
		} catch (err) {
			this.client.errorLog('Failed to connect to database', err);
		}
	}

	public query<T>(query: string, values: any[] = []): Promise<T> {
		if (!this.connected) return Promise.reject(new Error('Not connected to database'));
		return new Promise((resolve, reject) => {
			this.connection.query(query, values, (err, data) => {
				if (err) return reject(err);
				resolve(data);
			});
		});
	}

	public async getTopLeaderboard(game: string): Promise<LeaderboardItem[]> {
		try {
			const data = await this.query<LeaderboardItem[]>(`
				SELECT 
					l.score,
					u.name
				FROM
					leaderboards l
				LEFT JOIN
					users u
				ON
					u.userId = l.userId
				WHERE
					l.game = ?
				ORDER BY
					l.score DESC
				LIMIT 5
			`, [game]);
			return data;
		} catch (err) {
			this.client.errorLog('Failed to get top leaderboard', err);
			throw err;
		}
	}

	public async getUserScore(game: string, user: User): Promise<LeaderboardSingleUser> {
		try {
			const data = await this.query<LeaderboardSingleUser[]>(`
				SELECT 
					u.name,
					l.score,
					(
						SELECT
							count(*)
						FROM
							leaderboards
						WHERE
							score > (
								SELECT
									score
								FROM
									leaderboards x
								WHERE
									x.userId = l.userId
									AND 
									x.game = ?
							)
							AND
							game = ?
					) + 1 AS position
				FROM 
					leaderboards l
				LEFT JOIN
					users u
				ON
					u.userId = l.userId
				WHERE
					l.userId = ?
					AND
					l.game = ?
			`, [
				game,
				game,
				user.id,
				game,
			]);
			if (!data || !data.length) return data[0];
			data[0].self = true;
			return data?.[0];
		} catch (err) {
			this.client.errorLog('Failed to get user score', err);
			throw err;
		}
	}

	public async getUserLeaderboard(game: string, user: User): Promise<LeaderboardSingleUser[]> {
		try {
			const userScoreData = await this.getUserScore(game, user);
			if (!userScoreData) return [];
			const userAbove = await this.query<LeaderboardSingleUser[]>(`
				SELECT
					u.name,
					l.score,
					(
						SELECT
							count(*)
						FROM
							leaderboards
						WHERE
							score > (
								SELECT
									score
								FROM
									leaderboards x
								WHERE
									x.userId = l.userId
									AND 
									x.game = ?
							)
							AND
							game = ?
					) + 1 AS position
				FROM
					leaderboards l
				LEFT JOIN
					users u
				ON
					u.userId = l.userId
				WHERE
					l.score > ? AND l.userId != ? AND l.game = ?
				ORDER BY
					score ASC
				LIMIT 2
			`, [game, game, userScoreData.score, user.id, game]);
			const userBelow = await this.query<LeaderboardSingleUser[]>(`
				SELECT
					u.name,
					l.score,
					(
						SELECT
							count(*)
						FROM
							leaderboards
						WHERE
							score > (
								SELECT
									score
								FROM
									leaderboards x
								WHERE
									x.userId = l.userId
									AND 
									x.game = ?
							)
							AND
							game = ?
					) + 1 AS position
				FROM
					leaderboards l
				LEFT JOIN
					users u
				ON
					u.userId = l.userId
				WHERE
					l.score <= ? AND l.userId != ?
				ORDER BY
					score DESC
				LIMIT 2
			`, [game, game, userScoreData.score, user.id]);
			return [...userAbove.reverse(), userScoreData, ...userBelow];
		} catch (err) {
			this.client.errorLog('Failed to get selected users score', err);
			throw err;
		}
	}

	public async getQuiz(quizId: string): Promise<Quiz> {
		try {
			const data = await this.query<DatabaseQuizResponse[]>(`
				SELECT
					q.name,
					u.name AS owner,
					u.avatar AS ownerAvatar,
					u.discriminator AS ownerDiscriminator,
					cast(q.userId as char) AS ownerId,
					q.description,
					q.image,
					qc.name AS category,
					qd.name AS difficulty,
					qu.questionId,
					qu.question,
					a.answer,
					a.correct
				FROM quizzes q
				LEFT JOIN users u ON u.userId = q.userId
				LEFT JOIN questions qu ON qu.quizId = q.quizId
				LEFT JOIN answers a  ON a.questionId = qu.questionId
				LEFT JOIN quizCategories qc ON q.categoryId = qc.categoryId
				LEFT JOIN quizDifficulties qd ON q.difficultyId = qd.difficultyId
				WHERE q.displayId = ?
			`, [quizId]);

			if (!data.length) return;

			const usedQuestionIds: number[] = [];
			const quiz: Quiz = {
				quizId,
				owner: data[0].owner,
				ownerAvatar: data[0].ownerAvatar,
				ownerId: data[0].ownerId,
				ownerDiscriminator: data[0].ownerDiscriminator,
				name: data[0].name,
				description: data[0].description,
				image: data[0].image,
				category: data[0].category,
				difficulty: data[0].difficulty,
				questions: [],
			};
			data.forEach((question) => {
				if (!usedQuestionIds.includes(question.questionId)) {
					usedQuestionIds.push(question.questionId);
					quiz.questions.push({
						questionId: question.questionId.toString(),
						question: question.question,
						correctAnswers: [],
						incorrectAnswers: [],
					});
				}
				if (question.correct) {
					quiz.questions[quiz.questions.length - 1].correctAnswers.push(question.answer);
				} else {
					quiz.questions[quiz.questions.length - 1].incorrectAnswers.push(question.answer);
				}
			});
			return quiz;
		} catch (err) {
			this.client.errorLog('Failed to get quiz', err);
			throw err;
		}
	}

	async getUsersQuizzes(userId: string): Promise<QuizThumbnail[]> {
		try {
			return await this.query<QuizThumbnail[]>(`
				SELECT
					name,
					displayId as quizId
				FROM
					quizzes
				WHERE
					userId = ?
			`, [userId]);
		} catch (err) {
			this.client.errorLog('Failed to get users quizzes', err);
			throw err;
		}
	}

	async addInteractionLogs(interaction: CommandInteraction) {
		try {
			await this.query(`
				INSERT INTO interactionLogs (
					interactionId,
					userId,
					commandName
				) VALUES (
					?,
					?,
					?
				)
			`, [interaction.id, interaction.user.id, interaction.commandName]);
		} catch (err) {
			this.client.errorLog('Failed to add interaction to logs', err);
			throw err;
		}
	}

	async addToGames(gameName: string, playerOneId: string, playerTwoId: string): Promise<number> {
		try {
			const query = await this.query<OkPacket>(`
				INSERT INTO games (
					gameName,
					playerOneId,
					playerTwoId
				) VALUES (
					?,
					?,
					?
				)
			`, [gameName, playerOneId, playerTwoId]);
			return query.insertId;
		} catch (err) {
			if (err.code === 'ER_NO_REFERENCED_ROW_2') {
				// The opponent doesn't have an entry in the users table
				// This behaviour is expected so we don't log it
				throw new Error('Opponent does not exist');
			}
			this.client.errorLog('Failed to add game to database', err);
			throw err;
		}
	}

	async updateGameState(gameId: number, status: GameState): Promise<void> {
		try {
			await this.query(`
				UPDATE games
				SET gameState = ?
				WHERE gameId = ?
			`, [status, gameId]);
		} catch (err) {
			this.client.errorLog('Failed to update game status', err);
			throw err;
		}
	}

	async addUser(user: User): Promise<void> {
		try {
			await this.query(`
				INSERT INTO users (
					userId,
					name,
					discriminator,
					avatar
				) VALUES (
					?,
					?,
					?,
					?
				) ON DUPLICATE KEY UPDATE
					name = ?,
					discriminator = ?,
					avatar = ?
			`, [
				user.id,
				user.username,
				user.discriminator,
				user.avatar,
				user.username,
				user.discriminator,
				user.avatar,
			]);
		} catch (err) {
			this.client.errorLog('Failed to add user to database', err);
			throw err;
		}
	}
}
