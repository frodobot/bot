import CommandBase from '../core/CommandBase';

export interface ButtonObject {
	[id: string]: CommandBase;
}
