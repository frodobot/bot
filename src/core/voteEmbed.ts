import {MessageEmbed} from 'discord.js';

import {EMBEDCOLOR} from './GlobalConstants.js';

export const voteEmbed = new MessageEmbed()
	.setColor(EMBEDCOLOR)
	.setTitle('Thanks for voting for Frodo!')
	.setDescription('Voting for Frodo helps us out greatly and if you would like to further support us, be sure to vote for us everyday!')
	.addFields([
		{name: 'Want to add Frodo to your own server?', value: 'https://invite.frodo.fun', inline: true},
		{name: 'Want to vote again?', value: 'https://top.gg/bot/734746193082581084/vote', inline: true},
	]);
