// Handles all interactions after commands are run and stores command data

import {ButtonObject} from './ButtonObject';
import {FrodoClient} from './FrodoClient';
import CommandBase from './CommandBase.js';

export default class InteractionManager {
	client: FrodoClient;
	commandData: ButtonObject;

	constructor(client: FrodoClient) {
		this.client = client;
		this.commandData = {};
	}

	addCommand(interactionId: string, command: CommandBase) {
		this.commandData[interactionId] = command;
	}

	getCommand(interactionId: string): CommandBase {
		return this.commandData?.[interactionId];
	}

	deleteCommand(interactionId: string) {
		if (!this.commandData?.[interactionId]) return;
		delete this.commandData[interactionId];
	}
}
