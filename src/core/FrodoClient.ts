// Handles the base connection to Discord

import {Client, Collection, CommandInteraction, CommandInteractionOptionResolver, ButtonInteraction} from 'discord.js';

import {MessageHandler} from './ErrorHandling/MessageHandler.js';
import CommandRegister from './CommandRegister.js';
import InteractionManager from './InteractionManager.js';
import CommandBase from './CommandBase.js';
import {MailManager} from './MailManager.js';
import {makeEmail, makeGitLabIssue, makeUncaughtErrorEmail} from './MailManagerType.js';
import Database from './Database.js';
import {CommandMapStorage} from './Command.js';
import createLogger, {Logger} from '../../Logger.js';


import * as commands from '../commands/commands.js';
import * as events from '../events/events.js';

export class FrodoClient extends Client {
	commands: Collection<string, CommandMapStorage>;
	startTime: number;
	commandRegister: CommandRegister;
	interactionManager: InteractionManager;
	mailer: MailManager;
	database: Database;
	logger: Logger;

	constructor(args?) {
		super(args);
		this.logger = createLogger(`Shard${this.shard.ids[0]}`);
		this.commands = new Collection();
		this.startTime = Date.now();
		this.loadCommands();
		this.registerEvents();
		this.startDatabase();
		this.startMailer();
	}

	private async loadCommands() {
		this.debugLog('Registering commands');
		for (const command of Object.values(commands)) {
			this.debugLog(` -> Loading command ${command.name}`);
			const handler = command.handler.prototype instanceof CommandBase ? command.handler : command.handler.bind(this);
			const commandData = {data: command, run: handler};
			this.commands.set(command.name, commandData);
		}
	}

	private createCommandRegister() : FrodoClient {
		this.debugLog('Making Command Register');
		const commandArray = [];
		this.commands.forEach((command) => {
			commandArray.push(command.data);
		});

		this.commandRegister = new CommandRegister(commandArray, this);
		return this;
	}

	private startDatabase() {
		this.debugLog('Starting database');
		this.database = new Database(this);
	}

	private startMailer() {
		this.debugLog('Starting MailManager');
		this.mailer = new MailManager(this);
		this.listenForErrors();
	}

	private listenForErrors() {
		process.on('uncaughtException', async (error) => {
			this.errorLog('Uncaught Error', error);
			await this.mailer.sendMail({
				from: MailManager.SUBJECTS.UNCAUGHT_ERROR,
				to: process.env.EMAILRECIVERS,
				subject: `Frodo Bot Uncaught Error (${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()})`,
				text: makeUncaughtErrorEmail([
					['title', `Frodo has encountered the following error:`],
					['shard', this.shard.ids[0]?.toString()],
					['date', `${new Date()}`],
					['error', error.stack.split('\n').join('<br/>')],
				]),
			});
			if (process.env.GITLABISSUEEMAIL) {
				await this.mailer.sendMail({
					from: MailManager.SUBJECTS.UNCAUGHT_ERROR,
					to: process.env.GITLABISSUEEMAIL,
					subject: `[NEEDS REVIEWING] ${error.name} Error in Runtime`,
					text: makeGitLabIssue(error),
					textType: 'html',
				});
			}
			process.exit(1);
		});
	}

	public async sendErrorMail(error, interaction: CommandInteraction) {
		this.mailer.sendMail({
			from: MailManager.SUBJECTS.CAUGHT_ERROR,
			to: process.env.EMAILRECIVERS,
			subject: `Frodo Bot Caught Error (${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()})`,
			text: makeEmail([
				['title', 'Frodo has caught the following error:'],
				['shard', this.shard.ids[0]?.toString()],
				['date', `${new Date()}`],
				['command_name', interaction.commandName],
				['command_id', interaction.id],
				['error', error.stack.split('\n').join('<br/>')],
			]),
		});
	}

	public async connectToDiscord() {
		this.debugLog('Attempting to login to discord...');
		await this.login(process.env.TOKEN);
		this.debugLog('Logged into Discord');
		await this.createCommandRegister();
		this.debugLog('Creating interaction manager');
		this.interactionManager = new InteractionManager(this);
		this.user.setActivity('/help', {type: 'PLAYING'});
		this.finishDiscordLogin();
	}

	private async registerEvents() : Promise<FrodoClient> {
		this.debugLog('Registering events');
		for (const event of Object.values(events)) {
			this.debugLog(` -> Registering event ${event.name} (${event.identifier})`);
			this.on(event.name, event.handler.bind(this));
		}

		this.debugLog('Events registered');
		return this;
	}

	private finishDiscordLogin() {
		this.debugLog('Finished logging into Discord');
		if (this.commandRegister.processFinished) {
			this.completeFinishLogin();
			return;
		}
		this.commandRegister.setCompleteEvent(this.completeFinishLogin.bind(this));
	}

	private completeFinishLogin() {
		this.debugLog(`Started in ${this.timeSinceStart / 1000} second${this.timeSinceStart == 1000 ? '' : 's'}`);
	}

	public get timeSinceStart(): number {
		return Date.now() - this.startTime;
	}

	public debugLog(message: string) {
		this.logger.info(message);
	}

	public warnLog(message: string) {
		this.logger.warn(message);
	}

	public errorLog(message: string, error?: Error) {
		this.logger.error(message, error);
	}
}

export {MessageHandler as Message, CommandInteractionOptionResolver as Options, CommandInteraction as Interaction, ButtonInteraction};
