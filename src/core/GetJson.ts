import fetch from 'node-fetch';

/**
 * Fetch JSON from a URL
 * @param {string} url The URL to fetch the JSON from
 * @param {HeadersInit} headers The headers to send with the request
 * @return {Promise<resType>} The JSON object
 */
export default async function getJson<resType = any>(url: string, headers: HeadersInit = {}): Promise<resType> {
	const req = await fetch(url, {headers});
	const json = await req.json();
	return <resType> json;
}
