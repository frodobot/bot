export interface Quiz {
	quizId: string;
	owner: string;
	ownerAvatar: string;
	ownerId: string;
	ownerDiscriminator: number;
	name: string;
	image?: string;
	description: string;
	questions: Question[];
	category: string;
	difficulty: string;
}

interface Question {
	questionId: string;
	question: string;
	incorrectAnswers: string[];
	correctAnswers: string[];
}

export interface DatabaseQuizResponse {
	owner: string;
	ownerAvatar: string;
	ownerId: string;
	ownerDiscriminator: number;
	name: string;
	description: string;
	image: string | null;
	category: string;
	difficulty: string;
	questionId: number;
	question: string;
	answer: string;
	correct: number;
}

export interface QuizThumbnail {
	quizId: string;
	name: string;
}
