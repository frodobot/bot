export interface Modal {
	title: string;
	id: string;
	inputs: ModalInput[];
}

interface ModalInput {
	id: string;
	label: string;
	style: 'SHORT' | 'PARAGRAPH';
	minLength?: number;
	maxLength?: number;
	placeholder?: string;
	required?: boolean;
}
