export enum GameState {
	NOT_FINISHED,
	DRAW,
	PLAYER_ONE_WIN,
	PLAYER_TWO_WIN,
}
