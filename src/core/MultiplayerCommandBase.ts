import {GameState} from './GameState.js';
import CommandBase from './CommandBase.js';
import {CommandBaseOptions} from './CommandBaseOptions.js';
import {ButtonValidate} from './ButtonValidate.js';

import {ButtonInteraction, User} from 'discord.js';

export default class MultiplayerCommandBase extends CommandBase {
	playerOne: User;
	playerTwo: User;
	players: User[];
	gameInDb: boolean = false;

	gameId: number;

	/**
	 * Base class for multiplayer games
	 * @param {CommandBaseOptions} options The command base options (should be passed as an arguement and not pre-defined)
	 * @param {boolean} [random=true] Whether the game's players should be randomised or not (needs to be hardcoded)
	 */
	constructor(options: CommandBaseOptions, random: boolean = true) {
		super(options);

		const playersList = this.getRandomPlayers(random);
		if (!playersList) return;
		this.players = playersList.randomPlayers;
		this.playerOne = playersList.playerOne;
		this.playerTwo = playersList.playerTwo;

		this.onReady();
		this.addGameToDatabase();
	}

	async addGameToDatabase() {
		try {
			this.gameId = await this.client.database.addToGames(this.interaction.commandName, this.playerOne.id, this.playerTwo.id);
			this.gameInDb = true;
		} catch (err) {}
	}

	public onReady(): void {
		throw new Error('Method not implemented.');
	}

	/**
	 * Set the state of the game in the games table and is used mainly for anyltics
	 * @param {GameState} status The status of the game
	 */
	public async setGameState(status: GameState) {
		if (!this.gameInDb) return;
		try {
			await this.client.database.updateGameState(this.gameId, status);
		} catch (err) {}
	}

	/**
	 * Can be used in place of `validateButtonClick` in multiplayer games
	 * ```JS
	 * public validateButtonClick(buttonId: string, interaction: ButtonInteraction): ButtonValidate {
	 * 	return this.validateMultiplayerButtonClick(interaction, this.currentPlayer, this.players);
	 * }
	 * ```
	 * @param {ButtonInteraction} interaction The button interaction
	 * @param {User} currentPlayer The current player in the game
	 * @param {User[]} players The array of players in the game
	 * @return {ButtonValidate} Returns the action to be completed
	 */
	 public validateMultiplayerButtonClick(interaction: ButtonInteraction, currentPlayer: User, players: User[]): ButtonValidate {
		if (interaction.user === currentPlayer) return ButtonValidate.Run;
		const playerIds = players.map((i) => i.id);
		if (playerIds.includes(interaction.user.id)) return ButtonValidate.Ignore;
		return ButtonValidate.Message;
	}
}
