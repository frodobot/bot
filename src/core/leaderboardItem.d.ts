export interface LeaderboardItem {
	avatar?: string;
	score?: string;
	name?: string;
}
export interface LeaderboardSingleUser {
	score: number;
	name: string;
	position: number;
	self?: boolean;
}
