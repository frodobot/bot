import {Insult} from './Insult.d';
import getJson from '../../../core/GetJson.js';
import {insultBlacklist} from './insultBlacklist.js';

export default async function getInsult(): Promise<Insult> {
	// Problems with cache on insult api https://github.com/EvilInsultGenerator/website/issues/23
	// Random search param solves this issue
	const random = Math.round(Math.random() * 1000000);
	const insult = await getJson<Insult>(`https://evilinsult.com/generate_insult.php?lang=en&type=json&random=${random}`);
	if (insultBlacklist.includes(insult.number)) {
		return getInsult();
	}
	return insult;
}
