import {QuizLog} from './types.d';
import {Button} from './../../../core/Button.d';
import {EMBEDCOLOR} from './../../../core/GlobalConstants.js';
import {CommandBaseOptions} from '../../../core/CommandBaseOptions.js';
import CommandBase from '../../../core/CommandBase.js';
import {Quiz} from '../../../core/Quiz.d';
import getAvatar from '../../../core/GetAvatar.js';

import {MessageEmbed} from 'discord.js';

export default class Playquiz extends CommandBase {
	quizId: string;
	quiz: Quiz;
	questionNumber: number;
	correctAnswerIndexes: string[];
	score: number;
	answers: string[];
	quizLog: QuizLog[];

	constructor(options: CommandBaseOptions) {
		super(options);

		if (!this.client.database.connected) {
			this.message.edit({
				embeds: [
					new MessageEmbed({
						color: '#ff0000',
						description: 'There was an error when fetching data from our database. Please report the issue as soon as possible at https://help.frodo.fun',
					}),
				],
			});
			return;
		}

		this.quizId = this.options.getString('quizid');
		this.quizLog = [];
		if (!this.quizId) {
			this.sendUnkownQuizMessage(`You need to specify a quiz ID when running the command! To get a quiz ID, make a quiz at https://frodo.fun/quizzes`);
			return;
		}
		this.fetchQuiz();
	}

	async fetchQuiz() {
		try {
			this.quiz = await this.client.database.getQuiz(this.quizId);
			if (!this.quiz) {
				this.sendUnkownQuizMessage('The quiz ID you entered doesn\'t exist! To get a quiz ID, make a quiz at https://frodo.fun/quizzes');
				return;
			}

			this.questionNumber = 0;
			this.score = 0;
			this.updateMessage();
		} catch (err) {
			this.message.edit({
				embeds: [
					new MessageEmbed({
						color: '#ff0000',
						description: 'There was an error when fetching data from our database. Please report the issue as soon as possible at https://help.frodo.fun',
					}),
				],
			});
			return;
		}
	}

	async sendUnkownQuizMessage(text: string) {
		try {
			const quizzes = await this.client.database.getUsersQuizzes(this.interaction.user.id);
			if (quizzes.length === 0) {
				this.message.edit({
					content: text,
				});
				return;
			}
			const buttons: Button[] = [];
			quizzes.forEach((quiz, index) => {
				if (index > 4) return;
				buttons.push({
					id: `pq-${quiz.quizId}`,
					label: quiz.name,
					style: 'PRIMARY',
				});
			});
			this.message.edit({
				content: `${text}\n\nYour current quizzes are:\n${quizzes.map((quiz) => `**${quiz.quizId}** - ${quiz.name}`).join('\n')}\n\nClick a button below to play one of your quizzes!`,
				components: [
					this.makeButtonRow(...buttons),
				],
			});
		} catch (err) {
			this.message.edit({
				content: text,
			});
		}
	}

	getQuizEmbed(): MessageEmbed {
		return new MessageEmbed()
			.setColor(EMBEDCOLOR)
			.setTitle(this.quiz.name)
			.setDescription(`${this.quiz.description}\n\nCategory: ${this.quiz.category}\nDifficulty: ${this.quiz.difficulty}\nScore: ${this.score}`)
			.setThumbnail(this.quiz.image ? `https://ik.imagekit.io/frodo/quizImages/${this.quiz.image}` : undefined)
			.setFooter({text: `Created by ${this.quiz.owner}  |  Quiz ID: ${this.quizId}`, iconURL: getAvatar(this.quiz.ownerId, this.quiz.ownerAvatar, this.quiz.ownerDiscriminator)});
	}

	async updateMessage() {
		const question = this.quiz.questions[this.questionNumber];
		this.correctAnswerIndexes = [];
		const buttons: Button[] = [];
		this.answers = question.correctAnswers.concat(question.incorrectAnswers).sort(() => Math.random() - 0.5);
		this.answers.forEach((answer, index) => {
			if (index > 4) return;
			if (question.correctAnswers.includes(answer)) this.correctAnswerIndexes.push(index.toString());
			buttons.push({
				id: index.toString(),
				label: answer,
				style: 'PRIMARY',
			});
		});
		this.message.edit({
			content: null,
			embeds: [
				this.getQuizEmbed(),
				new MessageEmbed()
					.setColor(EMBEDCOLOR)
					.setTitle(`Question ${this.questionNumber + 1}/${this.quiz.questions.length}`)
					.setDescription(question.question),
			],
			components: [
				this.makeButtonRow(...buttons),
			],
		});
	}

	getFinalQuizDescription(): string {
		let description = `You got ${this.score} out of ${this.quiz.questions.length} questions correct!\n\n`;
		this.quizLog.forEach((log, index) => {
			description += `**${index + 1})** ${log.inputAnswer} ${log.correct ? '✅' : `❌ - The correct answer was ${log.correctAnswer}`}\n`;
		});
		return description;
	}

	public onButtonClick(buttonId: string): void {
		if (buttonId.startsWith('pq-')) {
			this.quizId = buttonId.split('-')[1];
			this.fetchQuiz();
			return;
		}

		if (this.correctAnswerIndexes.includes(buttonId)) this.score++;
		this.quizLog.push({
			inputAnswer: this.answers[parseInt(buttonId)],
			correct: this.correctAnswerIndexes.includes(buttonId),
			correctAnswer: this.answers[parseInt(this.correctAnswerIndexes[0])],
		});
		this.questionNumber++;
		if (this.questionNumber >= this.quiz.questions.length) {
			this.message.edit({
				embeds: [
					this.getQuizEmbed(),
					new MessageEmbed()
						.setColor(EMBEDCOLOR)
						.setTitle('Quiz finished!')
						.setDescription(this.getFinalQuizDescription())
						.setFooter({text: `Run /playquiz quizid:${this.quizId} to play again!`})
						.setTimestamp(),
				],
				components: [],
			});
			return;
		}
		this.updateMessage();
	}
}
