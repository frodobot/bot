import {Command} from './../../../core/Command.d';
import playquiz from './playquiz.js';

export const command: Command = {
	name: 'playquiz',
	description: 'Play one of your custom quizzes made on our website!',
	options: [
		{
			name: 'quizid',
			description: 'The ID of the quiz you would like to play',
			type: 'STRING',
			required: false,
		},
	],
	version: '1.0.0',
	handler: playquiz,
};
