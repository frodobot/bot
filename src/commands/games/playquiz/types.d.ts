export interface QuizLog {
	inputAnswer: string;
	correct: boolean;
	correctAnswer: string;
}
