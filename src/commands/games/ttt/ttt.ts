import {User, MessageActionRow, ButtonInteraction} from 'discord.js';

import MultiplayerCommandBase from '../../../core/MultiplayerCommandBase.js';
import {Button} from './../../../core/Button.d';
import {GridPlace, GridDimensions} from './types.js';
import {makeTttGrid} from './tttUtils.js';
import {ButtonValidate} from '../../../core/ButtonValidate';
import {GameState} from '../../../core/GameState.js';
import {CLEAREMOJI} from '../../../core/GlobalConstants.js';

export default class Ttt extends MultiplayerCommandBase {
	grid: GridPlace[][];
	isPlayerOne: boolean;
	currentPlayer: User;
	gridDimensions: GridDimensions;
	countToWin: number;

	onReady() {
		const size = Number(this.options.getString('size'));
		this.gridDimensions = {
			width: size || 3,
			height: size || 3,
		};

		const countToWin = Number(this.options.getString('counttowin'));
		this.countToWin = countToWin || size || 3;

		if (this.countToWin > this.gridDimensions.width) {
			this.message.edit('Count to win cannot be greater than the grid size!');
			return;
		}

		this.isPlayerOne = true;
		this.currentPlayer = this.players[0];
		this.grid = makeTttGrid(this.gridDimensions.width, this.gridDimensions.height);
		this.updateNormalMessage();
	}

	checkWinWithModifiers(column: number, row: number, columnModifier: number, rowModifier: number) {
		let streak = 0;

		for (let y = -this.countToWin; y < this.countToWin; y++) {
			const rowIndex = row + (y * rowModifier);
			const columnIndex = column + (y * columnModifier);
			const gridValue: GridPlace = this.grid[rowIndex]?.[columnIndex];
			if (gridValue === undefined) continue;

			if (gridValue == (this.isPlayerOne ? GridPlace.PlayerOne : GridPlace.PlayerTwo)) {
				streak++;
				if (streak == this.countToWin) return true;
			} else {
				streak = 0;
			}
		}

		return false;
	}

	getMessageButtons(disabled?: boolean): MessageActionRow[] {
		const buttons: MessageActionRow[] = [];
		this.grid.forEach((row, rowI) => {
			const rowButtons: Button[] = [];
			row.forEach((cell, cellI) => {
				const button: Button = {
					id: `${rowI}-${cellI}`,
					disabled: disabled || cell !== GridPlace.Empty,
					style: cell === GridPlace.Empty ? 'SECONDARY' : cell === GridPlace.PlayerOne ? 'PRIMARY' : 'SUCCESS',
				};

				if (cell === GridPlace.Empty) button.emoji = CLEAREMOJI.id;
				else button.label = cell === GridPlace.PlayerOne ? 'X' : 'O';

				rowButtons.push(button);
			});
			buttons.push(this.makeButtonRow(...rowButtons));
		});
		return buttons;
	}

	async updateMessage(extension: string, buttonsDisabled: boolean = false) {
		await this.message.edit({
			content: `${this.playerOne} challenged ${this.playerTwo} to a game of ttt!\nCount To Win: \`${this.countToWin}\`\n\n${extension}`,
			components: this.getMessageButtons(buttonsDisabled),
		});
	}

	async updateNormalMessage() {
		await this.updateMessage(`${this.currentPlayer}'s (${this.isPlayerOne ? 'X' : 'O'}) turn!`);
	}

	checkForWin(column, row): boolean {
		return (
			this.checkWinWithModifiers(column, row, 0, 1) ||
			this.checkWinWithModifiers(column, row, 1, 0) ||
			this.checkWinWithModifiers(column, row, 1, 1) ||
			this.checkWinWithModifiers(column, row, 1, -1)
		);
	}

	checkForDraw(): boolean {
		return this.grid.every((row) => row.every((cell) => cell !== GridPlace.Empty));
	}

	switchPlayer() {
		this.isPlayerOne = !this.isPlayerOne;
		this.currentPlayer = this.isPlayerOne ? this.players[0] : this.players[1];
	}

	public onButtonClick(buttonId: string): void {
		const [row, column] = buttonId.split('-').map(Number);
		this.grid[row][column] = this.isPlayerOne ? GridPlace.PlayerOne : GridPlace.PlayerTwo;

		if (this.checkForWin(column, row)) {
			this.finishCommand();
			this.updateMessage(`${this.currentPlayer} Won!`, true);
			this.setGameState(this.isPlayerOne ? GameState.PLAYER_ONE_WIN : GameState.PLAYER_TWO_WIN);
			this.finishCommand();
			return;
		}

		if (this.checkForDraw()) {
			this.finishCommand();
			this.updateMessage(`They drew!`, true);
			this.setGameState(GameState.DRAW);
			this.finishCommand();
			return;
		}

		this.switchPlayer();
		this.updateNormalMessage();
	}

	public validateButtonClick(buttonId: string, interaction: ButtonInteraction): ButtonValidate {
		return this.validateMultiplayerButtonClick(interaction, this.currentPlayer, this.players);
	}
}
