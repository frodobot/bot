import {hangmanModal, hangmanStages, inputButton} from './variables.js';
import CommandBase from '../../../core/CommandBase.js';
import {CommandBaseOptions} from '../../../core/CommandBaseOptions.js';
import {ButtonValidate} from '../../../core/ButtonValidate.js';

import {ButtonInteraction, ModalSubmitFieldsResolver, ModalSubmitInteraction} from 'discord.js';

export default class Hangman extends CommandBase {
	userIdWhoEnteredWord: string;
	word: string[];
	clientWord: string;
	displayWord: string[];
	stage: number;
	wrongGuesses: string[];
	correctGuesses: string[];
	stopped: boolean;

	constructor(options: CommandBaseOptions) {
		super(options);

		this.stage = 0;
		this.wrongGuesses = [];
		this.correctGuesses = [];
		this.stopped = false;
		this.showInputButtonMessage();
	}

	showInputButtonMessage() {
		this.message.edit({
			content: `Click the button below to enter a word!`,
			components: [this.makeButtonRow(inputButton)],
		});
	}

	async runGame(word: string) {
		this.clientWord = this.getWordForGame(word);
		this.word = this.clientWord.toLocaleLowerCase().split('');

		this.displayWord = [];
		for (let i = 0; i < this.word.length; i++) {
			this.displayWord.push('-');
		}
		await this.updateMessage();
		this.runGameLoop();
	}

	async runGameLoop() {
		const input = await this.getInput();
		if (this.stopped) return;
		const letter = input.toLocaleLowerCase().substring(0, 1);
		if (this.wrongGuesses.includes(letter) || this.correctGuesses.includes(letter)) {
			this.runGameLoop();
			return;
		}
		if (this.word.includes(letter)) {
			this.correctGuesses.push(letter);
			const letterIndex = this.getLetterOccurances(letter);
			letterIndex.forEach((index) => {
				this.displayWord[index] = this.clientWord[index];
			});
			if (this.displayWord.join('') === this.clientWord) {
				this.gameOver(true);
				return;
			}
		} else {
			this.wrongGuesses.push(letter);
			this.stage++;
			if (this.stage === hangmanStages.length - 1) {
				this.gameOver(false);
				return;
			}
		}
		await this.updateMessage();
		this.runGameLoop();
	}

	getLetterOccurances(letter: string): number[] {
		const occurances = [];
		this.word.forEach((letterInWord, index) => {
			if (letterInWord === letter) {
				occurances.push(index);
			}
		});
		return occurances;
	}

	async updateMessage() {
		await this.message.edit({
			content: `Word chosen by <@${this.userIdWhoEnteredWord}>\nEnter letters that you think are in the word\nGuess a letter by typing \`.g <letter>\`\n${hangmanStages[this.stage]}\n\n\`${this.displayWord.join('')}\`\nWrong Guesses: \`${this.wrongGuesses.join('') || 'None!'}\``,
			components: [],
		});
	}

	async getInput(): Promise<string> {
		const message = (await this.message.channel.awaitMessages({
			filter: (message) => message.author.id !== this.userIdWhoEnteredWord && message.content.startsWith('.g '),
			max: 1,
		})).first();
		if (this.stopped) return;
		if (!message) {
			this.sendError(new Error('No message was recived'));
			this.finishCommand();
			this.stopped = true;
			return;
		}
		await message.delete()
			.catch(() => {});
		return message.content.replace('.g ', '');
	}

	getWordForGame(word): string {
		return word.replace(/ /g, '').substring(0, 20);
	}

	async gameOver(win: boolean) {
		this.finishCommand();
		await this.message.edit({
			content: `Word chosen by <@${this.userIdWhoEnteredWord}>\n${hangmanStages[this.stage]}\n\n\`${this.displayWord.join('')}\`\nWrong Guesses: \`${this.wrongGuesses.join('') || 'None!'}\`\n${win ? `The word was guessed the word correctly!` : `The word was guessed the word incorrectly!\nThe word was \`${this.clientWord}\``}`,
			components: [],
		});
	}

	public validateButtonClick(buttonId: string, interaction: ButtonInteraction): ButtonValidate {
		if (buttonId === 'input') {
			interaction.showModal(this.makeModal(hangmanModal));
			return;
		}
	}

	public onModalSubmit(modalId: string, fields: ModalSubmitFieldsResolver, interaction: ModalSubmitInteraction): string | void {
		// Check if user has already submitted word
		if (this.clientWord) return 'Someone has already submitted a word!';

		this.userIdWhoEnteredWord = interaction.user.id;
		this.runGame(fields.getTextInputValue('word')?.trim());
	}
}
