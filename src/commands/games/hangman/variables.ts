import {Modal} from '../../../core/Modal.d';
import {Button} from './../../../core/Button.d';

export const hangmanStages = [
	'___\n|      |\n|    \n|    \n|      \n|    \n|',
	'___\n|      |\n|    :dizzy_face: \n|      | \n|      \n|    \n|',
	'___\n|      |\n|    :dizzy_face: \n|    /|\\ \n|      \n|    \n|',
	'___\n|      |\n|    :dizzy_face: \n|    /|\\ \n|      |\n|    \n|',
	'___\n|      |\n|    :dizzy_face: \n|    /|\\ \n|      |\n|    /\n|',
	'___\n|      |\n|    :dizzy_face: \n|    /|\\ \n|      |\n|    / \\\n|',
];
export const inputButton: Button = {
	label: 'Enter Word',
	id: 'input',
	style: 'PRIMARY',
};

export const hangmanModal: Modal = {
	title: 'Hangman',
	id: 'hangman',
	inputs: [
		{
			id: 'word',
			label: 'Word',
			placeholder: 'Your word should contain no spaces',
			style: 'PARAGRAPH',
			minLength: 1,
			maxLength: 20,
			required: true,
		},
	],
};
