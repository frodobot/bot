import {Command} from '../../../core/Command';
import hangman from './hangman.js';

export const command: Command = {
	name: 'hangman',
	description: 'A game of hangman',
	version: '2.0.0',
	handler: hangman,
};
