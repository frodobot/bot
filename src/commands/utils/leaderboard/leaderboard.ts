import {avaliableGames} from './avaliableGames.js';
import {EMBEDCOLOR} from './../../../core/GlobalConstants.js';
import {FrodoClient, Message, Options} from '../../../core/FrodoClient';

import {MessageEmbed} from 'discord.js';

export default async function(this: FrodoClient, message: Message, options: Options) {
	if (!this.database.connected) {
		message.edit({
			embeds: [
				new MessageEmbed({
					color: '#ff0000',
					description: 'A malfunction has occured in the database connection. Please try again later. If you think you have found an error, please report it at https://help.frodo.fun',
				}),
			],
		});
		return;
	}
	const game = options.getString('game');
	if (!avaliableGames.includes(game)) return;
	const embed = new MessageEmbed()
		.setColor(EMBEDCOLOR)
		.setTitle(`${game[0].toUpperCase() + game.slice(1)} Leaderboard`)
		.setTimestamp();

	try {
		const user = options.getUser('user');
		if (user) {
			embed.setTitle(`${user.username}'s ${game} Leaderboard`);
			const userLeaderboard = await this.database.getUserLeaderboard(game, user);
			if (!userLeaderboard || userLeaderboard.length === 0) {
				embed.setDescription(`${user.username} has not played ${game} yet!`);
			} else {
				embed.setDescription(userLeaderboard.map((item) => {
					if (item.self) {
						return `**${item.position}. ${item.name} - ${item.score}**`;
					} else {
						return `${item.position}. ${item.name} - ${item.score}`;
					}
				}).join('\n'));
			}
		} else {
			const leaderboard = await this.database.getTopLeaderboard(game);
			embed.setDescription(leaderboard.map((item, index) => `${index + 1}. ${item.name} - ${item.score}`).join('\n'));
		}
	} catch (err) {
		message.edit({
			embeds: [
				new MessageEmbed({
					color: '#ff0000',
					description: 'There was an error when fetching data from our database. Please report the issue as soon as possible at https://help.frodo.fun',
				}),
			],
		});
		return;
	}

	message.edit({
		content: `You can see a larger leaderboard at https://frodo.fun/leaderboard#${game}`,
		embeds: [embed],
	});
}
