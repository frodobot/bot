#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"

REQUIRED_PKG="jq"
PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $REQUIRED_PKG|grep "install ok installed")
echo Checking for $REQUIRED_PKG: $PKG_OK
if [ "" = "$PKG_OK" ]; then
  echo "$REQUIRED_PKG not installed, install with 'sudo apt-get install $REQUIRED_PKG'"
  exit 1
fi

NODE_PATH=$(which node)
JOBS_PATH="./../jobs/jobs.json"
NUMBER_OF_JOBS=$(($(cat "$JOBS_PATH" | jq '.[].name' | wc -l) - 1))
CRON_JOBS=""

for i in $(seq 0 $NUMBER_OF_JOBS)
do
	JOB_NAME=$(cat "$JOBS_PATH" | jq ".[$i].name" -r)
	JOB_FILE=$(cat "$JOBS_PATH" | jq ".[$i].file" -r)
	JOB_TRIGGER=$(cat "$JOBS_PATH" | jq ".[$i].trigger" -r)
	echo "Registering job $JOB_NAME with trigger $JOB_TRIGGER"
	if [ -z "JOB_NAME" ] || [ -z "JOB_FILE" ] || [ -z "JOB_TRIGGER" ]; then
		echo "Job $JOB_NAME is not configured correctly"
		exit 1
	fi
	if [ ! -f "./../dist/jobs/$JOB_FILE" ]; then
		echo "Job $JOB_NAME's file $JOB_FILE does not exist in jobs folder"
		exit 1
	fi

	FILE_PATH=$(realpath "./../dist/jobs/$JOB_FILE")
	CRON_JOBS+="$JOB_TRIGGER $NODE_PATH $FILE_PATH\n"
done

echo -e "$CRON_JOBS" | crontab
exit 0
