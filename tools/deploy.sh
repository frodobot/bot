#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ..

# Exit if a command fails
set -e


echo "Pulling latest changes..."
git pull

echo "Installing dependencies and compiling typescript..."
yarn build

echo "Registering cron jobs..."
bash ./tools/registerCronJobs.sh

echo "Restarting frodo..."
sudo restart-frodo

exit 0
