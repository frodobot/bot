import fs from 'fs';
import path from 'path';
import tar from 'tar';

import Job from './Job.js';
import {__dirname} from '../loadEnvVars.js';

interface ArchiveLogFileEntry {
	name: string,
	filename: string,
	date: Date,
}

class ArchiveLogsJob extends Job {
	logPath: string;

	constructor() {
		super('archiveLogs');
		if (!process.env.RUNTIME && !process.env.LOG_TO_FILE) {
			this.logger.warn('Log files are not being stored in the current config');
			this.exit();
			return;
		}
		this.logPath = process.env.LOG_PATH || path.join(__dirname, '../logs');
		this.runJob();
	}

	getFilesInDirectory(dir: string, filterStr: string): Promise<string[]> {
		return new Promise((resolve, reject) => {
			fs.readdir(dir, (err, files) => {
				if (err) {
					reject(err);
					return;
				}
				resolve(files.filter((file) => {
					return file.endsWith(filterStr);
				}));
			});
		});
	}

	getTimeToArchiveFrom(dateInterval: number): number {
		const now = new Date(Date.now() - dateInterval);
		now.setUTCHours(0, 0, 0, 0);
		return now.getTime();
	}

	formatDate(ISODate: string): Date {
		const splitDate = ISODate.split('');
		const lastHyphen = splitDate.lastIndexOf('-');
		splitDate[lastHyphen] = ':';
		const secondLastHyphen = splitDate.lastIndexOf('-');
		splitDate[secondLastHyphen] = ':';
		return new Date(splitDate.join(''));
	}

	createFolder(filename: string): Promise<void> {
		return new Promise((resolve, reject) => {
			const folderPath = path.join(this.logPath, filename);
			fs.mkdir(folderPath, {recursive: true}, (err) => {
				if (err) {
					reject(err);
					return;
				}
				resolve();
			});
		});
	}

	deleteFiles(files: string[]): Promise<void> {
		return new Promise((resolve, reject) => {
			for (const file of files) {
				fs.unlink(file, (err) => {
					if (err) {
						reject(err);
						return;
					}
				});
			}
			resolve();
		});
	}

	async archiveLogFiles(): Promise<void> {
		const files = await this.getAllLogFilesToArchive();
		if (files.length === 0) {
			this.logger.warn('No log files to archive');
			return;
		}

		this.logger.info(`Archiving ${files.length} log file${files.length > 1 ? 's' : ''}`);

		const today = new Date();
		const day = today.getUTCDate() - 1;
		const month = today.getUTCMonth() + 1;
		const year = today.getUTCFullYear();
		const archiveFile = path.join(this.logPath, 'archive', `${year}-${month >= 10 ? month : `0${month}`}-${day >= 10 ? day : `0${day}`}.tar.gz`);
		await this.createFolder('archive');
		await tar.create({
			gzip: true,
			cwd: this.logPath,
			file: archiveFile,
		}, files.map(({filename}) => filename));

		this.logger.info('Logs archived');
		this.logger.info('Deleting log files');

		await this.deleteFiles(files.map(({filename}) => path.join(this.logPath, filename)));
	}

	async getAllLogFilesToArchive(): Promise<ArchiveLogFileEntry[]> {
		// Reformat the date to be in the correct format for the Date API
		const files = await this.getFilesInDirectory(this.logPath, '.log');
		const mappedFiles = files.map((file) => {
			return {
				name: file.split('_')[1].replace('.log', ''),
				date: this.formatDate(file.split('_')[0]),
				filename: file,
			};
		});

		// Sort the logs by date
		mappedFiles.sort((a, b) => a.date.getTime() - b.date.getTime());

		// Sort the logs into an object by log type
		const logsByLogType: {
			[key: string]: ArchiveLogFileEntry[],
		} = {};
		mappedFiles.forEach((file) => {
			if (!logsByLogType[file.name]) logsByLogType[file.name] = [];
			logsByLogType[file.name].push(file);
		});

		// Remove the last occurence of each type of log
		Object.keys(logsByLogType).forEach((type) => {
			logsByLogType[type].pop();
		});

		// Add all types of logs back to one array
		const allFilesToArchive: ArchiveLogFileEntry[] = [];
		Object.keys(logsByLogType).forEach((type) => {
			allFilesToArchive.push(...logsByLogType[type]);
		});

		// Remove logs that are less than a day old
		const today = this.getTimeToArchiveFrom(0);
		this.logger.info(`Archiving logs older than ${new Date(today).toISOString()}`);
		const filteredFiles = allFilesToArchive.filter((file) => file.date.getTime() < today);

		return filteredFiles;
	}

	async deleteCompressedFiles(): Promise<void> {
		const compressedFiles = await this.getCompressedFilesToDelte();
		if (compressedFiles.length === 0) {
			this.logger.warn('No compressed log files to archive');
			return;
		}

		this.logger.info(`Deleting ${compressedFiles.length} compressed log file${compressedFiles.length > 1 ? 's' : ''}`);

		await this.deleteFiles(compressedFiles.map((file) => path.join(this.logPath, 'archive', file)));
	}

	async getCompressedFilesToDelte(): Promise<string[]> {
		const files = await this.getFilesInDirectory(path.join(this.logPath, 'archive'), '.tar.gz');
		const timeToArchiveFrom = this.getTimeToArchiveFrom(1000 * 60 * 60 * 24 * 30);

		this.logger.info(`Deleting compressed log files older than ${new Date(timeToArchiveFrom).toISOString()}`);

		// Files should be in the format <YEAR>-<MONTH>-<DAY>.tar.gz
		const filteredFiles = files.filter((file) => {
			file = file.replace(/\.tar\.gz$/, '');
			const [year, month, day] = file.split('-');
			const date = new Date(Number(year), Number(month) - 1, Number(day));
			return date.getTime() < timeToArchiveFrom;
		});

		return filteredFiles;
	}

	async runJob() {
		try {
			await this.connectToDatabase();
			const jobShouldRun = await this.shouldJobRun(1000 * 60 * 60 * 24);
			if (!jobShouldRun) {
				this.exit();
				return;
			}

			await this.archiveLogFiles();
			await this.deleteCompressedFiles();

			this.exit();
		} catch (err) {
			this.logger.error('Failed to archive logs', err);
			this.exit();
		}
	}
}

new ArchiveLogsJob();
