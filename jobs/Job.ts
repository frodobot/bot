// Basic class for all cron jobs to extend

import {Connection, createConnection} from 'mysql';
import createLogger, {Logger} from '../Logger.js';

import loadEnvVars from '../loadEnvVars.js';
loadEnvVars();

export default class Job {
	connection: Connection;
	jobId: string;
	connected: boolean = false;
	logger: Logger;

	/**
	 * Initalises the job
	 * @param {string} jobId The id of the job that will be used for last time run and logger
	 */
	constructor(jobId: string) {
		this.logger = createLogger(jobId);
		this.logger.info(`${jobId} job started`);
		this.jobId = jobId;
	}

	/**
	 * Function to check whether the job should run or not
	 * @param {number} expectedDelay The expected delay between runs
	 * @return {Promise<boolean>} Whether the job should run or not
	 */
	public async shouldJobRun(expectedDelay: number): Promise<boolean> {
		if (!process.env.RUNTIME) return true;
		try {
			const lastCompleted = await this.getWhenJobLastCompleted();
			const now = new Date().getTime();
			const timeSinceLastCompleted = now - lastCompleted;
			this.logger.info(`Time since job last completed: ${timeSinceLastCompleted}`);
			// Allow for a small margin of error
			const jobShouldRun = lastCompleted ? timeSinceLastCompleted > (expectedDelay - (60 * 1000)) : true;
			if (jobShouldRun) {
				await this.updateLastCompleted();
			} else {
				this.logger.warn(`Not running job, time since last completed is less than expected delay`);
			}
			return jobShouldRun;
		} catch (err) {
			this.logger.error('Failed to check if job should run', err);
			return false;
		}
	}

	private async updateLastCompleted(): Promise<void> {
		this.logger.info('Updating time job last completed');
		await this.dbQuery(`
			INSERT INTO botData (
				keyId,
				value
			) VALUES (
				?,
				?
			) ON DUPLICATE KEY UPDATE
				value = ?
		`, [`${this.jobId}LastCompleted`, Date.now().toString(), Date.now().toString()]);
	}

	private async getWhenJobLastCompleted(): Promise<number> {
		const result = await this.dbQuery<{
			value: string;
		}[]>(`
			SELECT
				value
			FROM
				botData
			WHERE
				keyId = ?
		`, [`${this.jobId}LastCompleted`]);
		return parseInt(result[0]?.value);
	}

	/**
	 * Execute a query within the database
	 * @param {string} query The SQL query to execute
	 * @param {any[]} values The values to be inserted into the query
	 * @return {Promise<T>} The result of the query
	 */
	public dbQuery<T>(query: string, values: any[] = []): Promise<T> {
		if (!this.connected) return Promise.reject(new Error('Not connected to database'));
		return new Promise((resolve, reject) => {
			this.connection.query(query, values, (err, data) => {
				if (err) return reject(err);
				resolve(data);
			});
		});
	}

	/**
	 * Connects to the database and resolves when connected
	 * @return {Promise<void>}
	 */
	public connectToDatabase(): Promise<void> {
		return new Promise((resolve, reject) => {
			if (this.connected) return;
			this.logger.info('Connecting to database');
			try {
				this.connection = createConnection({
					host: process.env.DB_SERVER,
					user: process.env.DB_USER,
					password: process.env.DB_PASSWORD,
					database: process.env.DB_DATABASE,
					port: parseInt(process.env.DB_PORT) || 3306,
					charset: 'utf8mb4',
				});
				this.connection.connect((err) => {
					if (err) {
						this.logger.error('Failed to connect to database', err);
						return reject(err);
					};
					this.connected = true;
					this.logger.info('Successfully connected to database');
					resolve();
				});
			} catch (err) {
				this.logger.error('Failed to connect to database', err);
				reject(err);
			}
		});
	}

	/**
	 * Exits the process, you must return straight after
	 */
	public exit(): void {
		this.logger.info(`${this.jobId} job finished`);
		this.logger.end();
		this.connection?.end();
		// Exits the process after a minute
		setTimeout(() => {
			process.exit(0);
		}, 60000);
	}
}
