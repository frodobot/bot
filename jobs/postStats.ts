import {Api} from '@top-gg/sdk';
import fetch from 'node-fetch';

import Job from './Job.js';

export default class PostStatsJob extends Job {
	api: Api;

	constructor() {
		super('postStats');
		if (!process.env.TOPGG_TOKEN) {
			this.logger.error('No top.gg token found (set TOPGG_TOKEN in .env)');
			this.exit();
			return;
		}
		this.api = new Api(process.env.TOPGG_TOKEN);
		this.runJob();
	}

	async getGuildCount(): Promise<number> {
		try {
			this.logger.info('Fetching guild count');
			const res = await fetch('https://frodo.fun/api/servers');
			const json = await res.json() as {message: string};
			this.logger.info(`Guild count: ${json.message}`);
			return parseInt(json.message);
		} catch (err) {
			this.logger.error('Failed to fetch guild count', err);
			return 0;
		}
	}

	async getShardCount(): Promise<number> {
		try {
			this.logger.info('Fetching shard count');
			const res = await fetch('https://frodo.fun/api/shards');
			const json = await res.json() as {count: string};
			this.logger.info(`Shard count: ${json.count}`);
			return parseInt(json.count);
		} catch (err) {
			this.logger.error('Failed to fetch shard count', err);
			return 1;
		}
	}

	async runJob(): Promise<void> {
		try {
			await this.connectToDatabase();
			const jobShouldRun = await this.shouldJobRun(1000 * 60 * 30);
			if (!jobShouldRun) {
				this.exit();
				return;
			}

			const guildCount = await this.getGuildCount();
			const shardCount = await this.getShardCount();

			this.logger.info('Posting stats to top.gg');
			await this.api.postStats({
				serverCount: guildCount,
				shardCount,
			});
			this.logger.info('Stats posted to top.gg');
			this.exit();
			return;
		} catch (err) {
			this.logger.error('Failed to post stats to top.gg', err);
			this.exit();
			return;
		}
	}
}

new PostStatsJob();
