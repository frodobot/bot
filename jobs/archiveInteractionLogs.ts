import Job from './Job.js';

class ArchiveInteractionLogsJob extends Job {
	constructor() {
		super('archiveInteractionLogs');
		this.runJob();
	}

	async runJob() {
		try {
			await this.connectToDatabase();
			const jobShouldRun = await this.shouldJobRun(1000 * 60 * 60 * 24);
			if (jobShouldRun) {
				await this.archiveLogs();
			} else {
				this.exit();
				return;
			}
		} catch (err) {
			this.exit();
			return;
		}
	}

	async archiveLogs() {
		try {
			this.logger.info('Fetching command names from logs older than a month');
			const commandsRun = await this.dbQuery<{
				commandName: string;
			}[]>(`
				SELECT DISTINCT
					commandName
				FROM
					interactionLogs
				WHERE
					timestamp < DATE_SUB(NOW(), INTERVAL 1 MONTH);
			`);
			for (const {commandName} of commandsRun) {
				this.logger.info(`Archiving logs for ${commandName}`);
				await this.dbQuery(`
					INSERT INTO
						oldInteractionLogs (
							commandName,
							count,
							startDateRange,
							endDateRange
						)
					VALUES (
						?,
						(
							SELECT
								COUNT(*)
							FROM
								interactionLogs
							WHERE
								commandName = ?
								AND
								timestamp < DATE_SUB(NOW(), INTERVAL 1 MONTH)
						),
						DATE_SUB(DATE_SUB(NOW(), INTERVAL 1 MONTH), INTERVAL 1 DAY),
						DATE_SUB(NOW(), INTERVAL 1 MONTH)
					)
				`, [commandName, commandName]);
				this.logger.info(`Archived logs for ${commandName}`);
				this.logger.info(`Deleting logs for ${commandName}`);
				await this.dbQuery(`
					DELETE FROM
						interactionLogs
					WHERE
						commandName = ?
						AND
						timestamp < DATE_SUB(NOW(), INTERVAL 1 MONTH);
				`, [commandName]);
				this.logger.info(`Deleted logs for ${commandName}`);
			}
			this.logger.info('Finished archiving logs');
			this.exit();
			return;
		} catch (err) {
			this.logger.error('Failed to archive logs', err);
			this.exit();
			return;
		}
	}
}

new ArchiveInteractionLogsJob();
