import winston, {Logger} from 'winston';
import chalk from 'chalk';

winston.addColors({
	info: 'blue',
	warn: 'yellow',
	error: 'red',
});

const loggerFormat = winston.format.printf(({level, message, timestamp, stack}) => {
	return `[${level}][${timestamp}] ${stack ? `${message}\n${stack}` : `${message}`}`;
});
const loggerFormatColour = winston.format.printf(({level, message, timestamp, label, stack}) => {
	return `[${chalk.green(label)}][${level}][${chalk.yellow(timestamp)}] ${stack ? `${message}\n${stack}` : `${message}`}`;
});

const loggerFormatOptions = (label: string) => ({
	level: 'info',
	format: winston.format.combine(
		winston.format.timestamp({
			format: 'HH:mm:ss',
		}),
		winston.format.colorize(),
		winston.format.label({label}),
		loggerFormatColour,
	),
	transports: [
		new winston.transports.Console(),
	],
});
const loggerFormatOptionsWithFile = (label: string) => ({
	level: 'info',
	format: winston.format.combine(
		winston.format.timestamp({
			format: 'HH:mm:ss',
		}),
		winston.format.errors({stack: true}),
	),
	transports: [
		new winston.transports.Console({
			format: winston.format.combine(
				winston.format.colorize(),
				winston.format.label({label}),
				loggerFormatColour,
			),
		}),
		new winston.transports.File({
			filename: `${process.env.LOG_PATH || './logs'}/${new Date().toISOString().replace(/:/g, '-')}_${label}.log`,
			format: winston.format.combine(
				loggerFormat,
			),
		}),
	],
});

export default function createLogger(label: string): Logger {
	const logger = winston.createLogger(process.env.RUNTIME || process.env.LOG_TO_FILE ? loggerFormatOptionsWithFile(label) : loggerFormatOptions(label));
	logger.info('Logger created');
	return logger;
}

export {Logger};
